import xbmcaddon
from threading import Timer
from xbmcgui import WindowXMLDialog

# From script.keymap
class KeyListener(WindowXMLDialog):
  TIMEOUT = 5

  def __new__(cls):
    gui_api = tuple(map(int, xbmcaddon.Addon('xbmc.gui').getAddonInfo('version').split('.')))
    file_name = "DialogNotification.xml" if gui_api >= (5, 11, 0) else "DialogKaiToast.xml"
    return super(KeyListener, cls).__new__(cls, file_name, "")

  def __init__(self):
    """Initialize key variable."""
    self.key = None

  def onInit(self):
    def do():
      self.getControl(401).addLabel("Press a key")
      self.getControl(402).addLabel("Timeout in %.0f seconds..." % self.TIMEOUT)
    try:
      do()
    except AttributeError:
      do()

  def onAction(self, action):
    code = action.getButtonCode()
    self.key = None if code == 0 else str(code)
    self.close()

  @staticmethod
  def record_key():
    dialog = KeyListener()
    timeout = Timer(KeyListener.TIMEOUT, dialog.close)
    timeout.start()
    dialog.doModal()
    timeout.cancel()
    key = dialog.key
    del dialog
    return key