import xbmc
import xbmcaddon
from xbmcgui import Dialog, WindowXMLDialog
from xbmcvfs import exists, mkdir, File

from keylistener import KeyListener

import json

Addon = xbmcaddon.Addon()

addon_data_path = Addon.getAddonInfo("profile")
json_file_path = addon_data_path + "keymap.json"

keymaps_path = "special://profile/keymaps/"
xml_file_path = keymaps_path + "remote-config.xml"

separator = "="*10


#
# Keymap structs
#

def default_keymap() -> list:
  keymap = []
  keymap.append({"action": "Select", "name": "Ok", "keys": []})
  keymap.append({"action": "Back", "keys": []})
  keymap.append(None)
  keymap.append({"action": "Up", "keys": []})
  keymap.append({"action": "Down", "keys": []})
  keymap.append({"action": "Left", "keys": []})
  keymap.append({"action": "Right", "keys": []})
  keymap.append(None)
  keymap.append({"action": "ContextMenu", "name": "Context menu", "keys": []})
  keymap.append({"action": "Info", "keys": []})
  keymap.append(None)
  keymap.append({"action": "BackSpace", "name": "SMS-style numpad backspace", "keys": []})
  keymap.append(None)
  keymap.append({"action": "VolumeUp", "name": "Volume up", "keys": []})
  keymap.append({"action": "VolumeDown", "name": "Volume down", "keys": []})
  keymap.append(None)
  keymap.append({"action": "PlayPause", "name": "Play/Pause", "keys": []})
  keymap.append({"action": "Stop", "name": "Stop", "keys": []})
  keymap.append({"action": "FullScreen", "name": "Switch between menus/player", "keys": []})
  keymap.append(None)
  keymap.append({"action": "Seek(15)", "name": "Seek +15 sec", "keys": []})
  keymap.append({"action": "Seek(-15)", "name": "Seek -15 sec", "keys": []})
  keymap.append({"action": "Seek(120)", "name": "Seek +2 min", "keys": []})
  keymap.append({"action": "Seek(-120)", "name": "Seek -2 min", "keys": []})
  keymap.append(None)
  keymap.append({"action": "SubtitleDelayPlus", "name": "Increase subtitle delay", "keys": []})
  keymap.append({"action": "SubtitleDelayMinus", "name": "Decrease subtitle delay", "keys": []})
  keymap.append(None)
  keymap.append({"action": "MoveItemUp", "name": "Move item up", "keys": []})
  keymap.append({"action": "MoveItemDown", "name": "Move item down", "keys": []})
  keymap.append(None)
  keymap.append({"action": "noop", "name": "Disabled", "keys": []})
  keymap.append(None)
  return keymap

def key_name(key: dict) -> str:
  if "name" in key:
    return key["name"]
  else:
    return key["action"]

def make_xml(keymap: list) -> str:
  xml = ""
  xml += "<keymap>\n"
  xml += "  <global>\n"
  xml += "    <keyboard>\n"
  for el in keymap:
    if isinstance(el, dict):
      name = key_name(el)
      action = el["action"]
      for id in el["keys"]:
        xml += f"      <!-- {name} --><key id=\"{id}\">{action}</key>\n"
    else:
      # Separator
      xml += "\n"
  xml += "    </keyboard>\n"
  xml += "  </global>\n"
  xml += "</keymap>\n"
  return xml


#
# File manipulation
#

def load_keymap() -> list:
  with File(json_file_path, "r") as f:
    s = f.read()
  if len(s) > 0:
    return json.loads(s)
  else:
    return []

def save_keymap(keymap: list) -> None:
  if not exists(addon_data_path):
    mkdir(addon_data_path)
  with File(json_file_path, "w") as f:
    f.write(json.dumps(keymap))

def write_xml(xml: str) -> None:
  if not exists(keymaps_path):
    mkdir(keymaps_path)
  with File(xml_file_path, "w") as f:
    f.write(xml)


#
# Gui
#

def edit_key_dialog(key: dict) -> dict:
  edit_list = []
  i = 0
  for code in key["keys"]:
    edit_list.append(f"{code} (Press to remove)")
    i += 1
  edit_list.append("Add key")
  index_add = i
  i += 1
  edit_list.append("Clear")
  index_clear = i
  i += 1
  edit_list.append("Reset to default")
  index_reset = i
  i += 1

  result = Dialog().select(f"Editing key: {key_name(key)}", edit_list)

  if result == index_add:
    # Get key from user
    code = KeyListener.record_key()
    if code is not None:
      key["keys"].append(int(code))
  elif result == index_clear:
    # Clear
    key["keys"] = []
  elif result == index_reset:
    # Reset to default
    for el in default_keymap():
      if isinstance(el, dict):
        if el["action"] == key["action"]:
          key["keys"] = el["keys"]
          break
  elif result >= 0 and result < len(key["keys"]):
    # Remove selected keycode
    key["keys"].pop(result)

  return key

if (__name__ == '__main__'):
  repeat = True
  while repeat:
    repeat = False

    keymap = load_keymap()
    if len(keymap) == 0:
      keymap = default_keymap()

    # Show main list
    main_list = []

    indexed = dict()
    i = 0
    for el in keymap:
      if isinstance(el, dict):
        indexed[i] = el
        name = key_name(el)
        main_list.append(f"Edit action: {name}")
      else:
        main_list.append(separator)
      i += 1

    main_list.append("Add custom action")
    index_custom_action = i
    i += 1
    main_list.append(separator)
    i += 1
    main_list.append("Clear all")
    index_clear_all = i
    i += 1
    main_list.append("Reset all to default")
    index_reset_all = i
    i += 1

    result = Dialog().select("Remote key overrides configurator", main_list)

    if result == index_custom_action:
      new_action = Dialog().input("Enter action name (https://kodi.wiki/view/Action_IDs)")
      if len(new_action) > 0:
        keymap.append({"action": new_action, "keys": []})
      save_keymap(keymap)
      xml = make_xml(keymap)
      write_xml(xml)
      repeat = True
    elif result == index_reset_all:
      keymap = default_keymap()
      save_keymap(keymap)
      xml = make_xml(keymap)
      write_xml(xml)
      repeat = True
    elif result == index_clear_all:
      for el in keymap:
        if isinstance(el, dict):
          el["keys"] = []
      save_keymap(keymap)
      xml = make_xml(keymap)
      write_xml(xml)
      repeat = True
    elif result in indexed:
      new_key = edit_key_dialog(indexed[result])
      #update in keymap
      for el in keymap:
        if isinstance(el, dict):
          if el["action"] == new_key["action"]:
            el["keys"] = new_key["keys"]
            break
      save_keymap(keymap)
      xml = make_xml(keymap)
      write_xml(xml)
      repeat = True
      pass
    else:
      # Exit
      xbmc.executebuiltin("action(reloadkeymaps)")
